from prog4_2 import StackMachine
from prog4_1 import Tokenizer,Parser
import sys 

def main():
    print("Assignment #4-3, Mark Park, mark619park@gmail.com")
    strings = [] 
    with open(sys.argv[1]) as test:
        token = test.readlines()
    token = [x.strip() for x in token]
    
    for x in token: #list of strings of input
        try: 
            Token = Tokenizer(x)
            strings.append(Token)   
        except ValueError as msg: 
            print(msg) 
            sys.exit() 
    for x in strings: 
        try: 
            Parser(x)

        except ValueError as e:
            print(e)
            sys.exit()
    
    SM = StackMachine()
    i = 0
    while (SM.CurrentLine < len(strings)):
        try:
            result = SM.Execute(strings[SM.CurrentLine])
            if result != None:
                print(result)
                i += 1 
        except IndexError as err:      
            FinlString = " ".join(strings[i])                   #error msg 
            print("Line " + str(i+1) + ": '" + FinlString + "' caused " + str(err)) 
            sys.exit() 
    print("Program terminated correctly") 
                       




if __name__ == "__main__": 
    main() 

    