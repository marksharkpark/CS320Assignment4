import sys


def Tokenizer(x):
    right = []
    strtok = x.split()
    for x in range(len(strtok)):                     #check contents of user input with valid tokens
        if strtok[x] == "pop":   
            right.append(strtok[x])                                #store into a list and then return the list.    
        elif strtok[x] == "push":
            right.append(strtok[x])  
        elif strtok[x] == "add":
            right.append(strtok[x])  
        elif strtok[x] == "sub":
            right.append(strtok[x])
        elif strtok[x] == "mul": 
            right.append(strtok[x])
        elif strtok[x] == "div": 
            right.append(strtok[x])
        elif strtok[x] == "mod":
            right.append(strtok[x])
        elif strtok[x] == "skip":
            right.append(strtok[x])  
        elif strtok[x] == "save":
            right.append(strtok[x])  
        elif strtok[x] == "get":
            right.append(strtok[x])    
        else:
            try:
                int(strtok[x])
                right.append(strtok[x]) 
            except:
                raise ValueError("Unexpected token: " + strtok[x])       
    return right


def Parser(right):
   if len(right) == 1:
        if right[0] == "pop": 
            pass
        elif right[0] == "add": 
            pass
        elif right[0] == "sub":
            pass
        elif right[0] == "mul": 
            pass
        elif right[0] == "div": 
            pass
        elif right[0] == "mod":
            pass
        elif right[0] == "skip":
            pass
        else:
            raise ValueError("Parse Error: " + " ".join(right))  
   elif len(right) == 2:
        if right[0] == "save":
            try:
                int(right[1]) 
            except:
                raise ValueError("Parse error: " + " ".join(right))  
            
            pass
        elif right[0] == "get":
            try:
                int(right[1]) 
            except:
                raise ValueError("Parse error: " + " ".join(right))  
            pass
        elif right[0] == "push":
            try:
               
                int(right[1]) 
            except:
                raise ValueError("Parse Error: " + " ".join(right))           
            pass
   else: 
        raise ValueError('Parse error: ' + " ".join(right))
