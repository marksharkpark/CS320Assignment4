import sys 

class StackMachine:
    def __init__(self):
        self.stack = [] 
        self.values = [] 
        self.save = {} #for save and get properties
        self.CurrentLine = 0
    def Execute(self,right): 
            if right[0] == "pop": 
                if len(self.stack) > 0: 
                    self.CurrentLine += 1 
                    return self.stack.pop()  
                else: 
                    raise IndexError("Invalid Memory Access") 
            if right[0] == "add":
                if len(self.stack) > 1: 
                    add1 = self.stack.pop()
                    add2 = self.stack.pop() 
                    add3 = add2 + add1
                    self.stack.append(add3) 
                    self.CurrentLine += 1 
                else: 
                    raise IndexError("Invalid Memory Access") 
            if right[0] == "sub":
                if len(self.stack) > 1:
                    sub1 = self.stack.pop()
                    sub2 = self.stack.pop() 
                    sub3 = sub1 - sub2
                    self.stack.append(sub3)
                    self.CurrentLine += 1 
                else: 
                    raise IndexError("Invalid Memory Access")   
                #pops two, subtracts second from first, pushes result 
            if right[0] == "push": 
                    self.stack.append(int(right[1]))  
                    self.CurrentLine += 1 
            

            if right[0] == "div":
                if len(self.stack) > 1: 
                    div1 = self.stack.pop()
                    div2 = self.stack.pop() 
                    div3 = div2/div1 
                    self.stack.append(div3) 
                    self.CurrentLine += 1 
                else: 
                    raise IndexError("Invalid Memory Access") 
                #pops two, divides the first by the second, pushes result 
            if right[0] == "mul":
                if len(self.stack) > 1:
                    mul1 = self.stack.pop()
                    mul2 = self.stack.pop() 
                    mul3 = mul1 * mul2 
                    self.stack.append(mul3) 
                    self.CurrentLine += 1 
                else: 
                    raise IndexError("Invalid Memory Access") 
                #pops two, multiplies, pushes result
            if right[0] == "mod":
                if len(self.stack) > 1: 
                    mod1 = self.stack.pop() 
                    mod2 = self.stack.pop()
                    mod3 = mod1 % mod2 
                    self.stack.append(mod3)
                    self.CurrentLine += 1 
                else: 
                    raise IndexError("Invalid Memory Access") 
                #pops two, remainder of first divided by second, pushes result 
            if right[0] == "skip":
                if len(self.stack) > 1:
                    skip1 = self.stack.pop()
                    skip2 = self.stack.pop() 
                    if (skip1 == 0):
                        self.CurrentLine += skip2
                    self.CurrentLine += 1
                else:
                    raise IndexError("Invalid Memory Access")
                
            if right[0] == "save": 
                if len(self.stack) > 0: 
                    save1 = self.stack.pop()
                    self.save[int(right[1])] = save1 
                    self.CurrentLine += 1 
                #future retrieval in save list 
                else:
                    raise IndexError("Invalid Memory Access") 

            if right[0] == "get":
                if not (int(right[1]) in self.save):  
                    raise IndexError("Invalid Memory Access") 
                self.stack.append(self.save[int(right[1])])
                self.CurrentLine += 1 
            else: 
                pass
             

                   
    
         